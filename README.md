**Learn how XML and JSON can be parsed with Express JS using the xml2js module**

---

## Installation

We have a few prequisites before you set up this project

1. Download the Node 14.15.4 LTS from https://nodejs.org/en/
2. Create an empty folder on your hard drive
3. Open Node.js command prompt and navigate to the empty folder created in step 2 using the command point. For reference on how to change directories using your windows 10 command panel see the following link. https://www.howtogeek.com/659411/how-to-change-directories-in-command-prompt-on-windows-10/
4. Once you have navigated to the folder type the following. npm install @lacelled/xml-json-parser. This will install the required modules
5. Once you have installed them move to the node_modules > @lacelled > xml-json-parser and move all the files out to your main original folder.
6. Go back to your console and type npm start. You will see in the console Example app listening at http://localhost:3000
7. Navigate to that page on your preferred browser and voila, you are ready to view it!
